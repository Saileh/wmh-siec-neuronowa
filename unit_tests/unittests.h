/*
 * Created by Albert Malewski. Dec 5th 2015.
 */

#ifndef UNITTESTS_H
#define UNITTESTS_H

#include "../src/neuron.h"
#include "../src/neurallayer.h"
#include "../src/neuralnetwork.h"

#include <vector>
#include <iostream>
#include <assert.h>

using namespace std;
using namespace boost;

/* Testy klasy Neuron */
void superTestGrzeska();
void testof_neuron_setOutput();
void testof_neuron_setInputs();

/* Testy klasy NeuralLayer */
void testof_neurallayer_connect();
void testof_neurallayer_connectIn();
void testof_neurallayer_connectIn2();
void testof_neurallayer_connectInFirstLayer();
void testof_neurallayer_connectOut();
void testof_neurallayer_aktywuj();
void testof_neurallayer_aktywuj2();
void testof_neurallayer_aktywuj3();

/* Testy klasy NeuralNetwork */
void testof_neuralnetwork_aktywuj();
void testof_neuralnetwork_aktywuj2();
void testof_neuralnetwork_aktywuj3();

/* Testy uczenia sieci */
void testof_neuralnetwork_propagujBlad();
void wyznaczanie_czasu_uczenia_sieci();

#endif // UNITTESTS_H
