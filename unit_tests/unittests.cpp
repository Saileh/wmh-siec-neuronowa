/*
 * Created by Albert Malewski. Dec 5th 2015.
 */

#include "unittests.h"

/* Testy klasy Neuron */

std::tuple<vector<double>, vector<double>> inicjalizacjaNeuronu()
{
    vector<double> wagi, we;
    for (int i = 0; i<2; i++) {
        wagi.push_back(1);
        we.push_back(1);
    }
    return std::make_tuple(wagi, we);
}

// Przebieg testu:
// tworzy neuron z funkcja akt liniowa, wagami i wektorem we - wekt wy to null
// sprawdza, czy taki wektor poprawnie dziala (powinien wykonywac zwykle dodawanie liczb)
void superTestGrzeska()
{
    std::cout << "###################" << endl;
    std::cout << "Przed Panstwem super test Grzeska!!!!!\n*******************" << std::endl;
    // przyg danych
    vector<double> wagi, we;
    std::tie(wagi, we) = inicjalizacjaNeuronu();
    double result = 1000000;
    // akcje
    Neuron* n = new Neuron(string("liniowa"), wagi, &we, nullptr);
    result = n->aktywuj();
    //asercja
    assert (result == we[0]+we[1]);
    std::cout << "PASSSSS!!!!!\n*******************\n\n" << std::endl;
}

// Przebieg testu:
// tworzy neuron z funkcja akt liniowa, wagami i wektorem we - wekt wy to null
// ustawia potem wektor wy i sprawdza, czy wyniki sie do niego zapisuja
void testof_neuron_setOutput()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neuron_setOutput!!!!!\n**********" << std::endl;
    // przyg danych
    vector<double> wagi, we;
    std::tie(wagi, we) = inicjalizacjaNeuronu();
    double result = 1000000;
    // akcje
    Neuron* n = new Neuron(string("liniowa"), wagi, &we, nullptr);
    n->setOutput(&result);
    n->aktywuj();
    // asercja
    assert (result == we[0]+we[1]);
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

// Przebieg testu:
// tworzy neuron z funkcja akt liniowa, wagami i - wekt we i wy to null
// ustawia potem wektory we oraz wy i sprawdza, czy sa one poprawnie obslugiwane
// TODO: Czy potrzebujemy funkcji setOutput i setOutput? Konstruktor nie wystarczy?
void testof_neuron_setInputs()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neuron_setInputs!!!!!\n**********" << std::endl;
    // przyg danych
    vector<double> wagi, we;
    std::tie(wagi, we) = inicjalizacjaNeuronu();
    double result = 1000000;
    // akcje
    Neuron* n = new Neuron(string("liniowa"), wagi, nullptr, nullptr);
    n->setOutput(&result);
    n->setInputs(&we);
    n->aktywuj();
    // asercja
    assert (result == we[0]+we[1]);
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

/* Testy klasy NeuronLayer */

void testof_neurallayer_connectIn()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_connectIn!!!!!\n**********" << std::endl;
    
    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    std::vector<double> v;
    v.resize(2);
    v[0] = 1.0;
    v[1] = 2.0;
    NeuralLayer::connectIn(n1, &v);
    for (int i=0; i<2; i++) {
        for (int j=0; j<2; j++) {
            std::cout << "i: " << i;
            std::cout << " j: " << j << endl;
            assert(n1->getNeuron(i)->getInput(j) == v[j]);
        }
    }
    
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_connectIn2()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_connectIn2!!!!!\n**********" << std::endl;
    
    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    NeuralLayer* n2 = new NeuralLayer(std::string("liniowa"), 3, 2);
    NeuralLayer::connect(n1, n2);

    for (int i=0; i<2; i++) {
        for (int j=0; j<3; j++) {
            std::cout << "i: " << i;
            std::cout << " j: " << j << endl;
            std::cout << "out: " << *n1->getNeuron(i)->getOutput();
            std::cout << " in: " << n2->getNeuron(j)->getInput(i) << endl;
            assert(*n1->getNeuron(i)->getOutput() == n2->getNeuron(j)->getInput(i));
        }
    }
    
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_connectOut()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_connectOut!!!!!\n**********" << std::endl;

    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    std::vector<double> v;
    v.resize(2);
    v[0] = 1.0;
    v[1] = 2.0;
    NeuralLayer::connectOut(n1, &v);
    for (int i=0; i<2; i++) {
        std::cout << "i: " << i;
        std::cout << " out: " << *n1->getNeuron(i)->getOutput() << endl;
        //assert(*n1->getNeuron(i)->getOutput() == v[i]);
    }

    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_connect()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_connect!!!!!\n**********" << std::endl;
    
    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    NeuralLayer* n2 = new NeuralLayer(std::string("liniowa"), 3, 2);

    NeuralLayer::connect(n1, n2);


    for (int i=0; i<2; i++) {
        for (int j=0; j<3; j++) {
            assert(n1->getNeuron(i)->getNextNeuron(j) == n2->getNeuron(j));
        }
    }

    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_connectInFirstLayer()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_connectInFirstLayer!!!!!\n**********" << std::endl;

    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    std::vector<double> v;
    v.resize(2);
    v[0] = 1.0;
    v[1] = 2.0;
    NeuralLayer::connectInFirstLayer(n1, &v);
    for (int i=0; i<2; i++) {
        std::cout << "i: " << i;
        assert(n1->getNeuron(i)->getInput(i) == v[i]);
    }

    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_aktywuj()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_aktywuj!!!!!\n**********" << std::endl;
        vector<double> wagi, we, wy;
    for (int i=0; i<2; i++) {
        wagi.push_back(1);
        we.push_back(1);
        wy.push_back(10000);
    }
    
    NeuralLayer* n = new NeuralLayer(std::string("liniowa"), 2, 2);
    n->setWagesToAllNeurons(wagi);
    NeuralLayer::connectInFirstLayer(n, &we);
    NeuralLayer::connectOut(n, &wy);
    n->aktywuj();
    
    for (int i=0; i<wy.size(); i++) {
        cout << "wy_" << i;
        cout << " : " << wy[i] << endl;
        assert(wy[i] == we[0] + we[1]);
    }
    
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_aktywuj2()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_aktywuj2!!!!!\n**********" << std::endl;
        vector<double> wagi, we, wy;
    for (int i=0; i<2; i++) {
        wagi.push_back(1);
        we.push_back(1);
    }
    wy.push_back(999999);
    
    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    NeuralLayer* n2 = new NeuralLayer(std::string("liniowa"), 1, 2);
    n1->setWagesToAllNeurons(wagi);
    n2->setWagesToAllNeurons(wagi);
    NeuralLayer::connectInFirstLayer(n1, &we);
    NeuralLayer::connect(n1, n2);
    NeuralLayer::connectOut(n2, &wy);
    n1->aktywuj();
    n2->aktywuj();

    cout << "wy: " << wy[0] << endl;
    assert(wy[0] == 2*we[0] + 2*we[1]);
    
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

void testof_neurallayer_aktywuj3()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neurallayer_aktywuj3!!!!!\n**********" << std::endl;
    vector<double> wagi, we, wy;
    for (int i=0; i<2; i++) {
        wagi.push_back(1);
        we.push_back(1);
    }
    wy.push_back(999999);
    
    NeuralLayer* n1 = new NeuralLayer(std::string("liniowa"), 2, 2);
    NeuralLayer* n2 = new NeuralLayer(std::string("liniowa"), 1, 2);
    n1->setWagesToAllNeurons(wagi);
    n2->setWagesToAllNeurons(wagi);
    NeuralLayer::connectInFirstLayer(n1, &we);
    NeuralLayer::connect(n1, n2);
    NeuralLayer::connectOut(n2, &wy);
    n1->aktywuj();
    n2->aktywuj();

    cout << "wy: " << wy[0] << endl;
    assert(wy[0] == 2*we[0] + 2*we[1]);
    
    we[0] = 2;
    we[1] = 3;
    
    n1->aktywuj();
    n2->aktywuj();

    cout << "wy: " << wy[0] << endl;
    assert(wy[0] == 2*we[0] + 2*we[1]);
    
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

/* Testy klasy NeuronNetwork */

void testof_neuralnetwork_aktywuj()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neuralnetwork_aktywuj!!!!!\n**********" << std::endl;
    vector<double> wagi, we, wy;
    for (int i=0; i<2; i++) {
        wagi.push_back(1);
        we.push_back(1);
    }
    wy.push_back(999999);
    
    NeuralNetwork* n = new NeuralNetwork(std::string("liniowa"), std::vector<int>{2, 1}, &we, &wy);
    for (int i=0; i<2; i++) {
        n->getLayer(i)->setWagesToAllNeurons(wagi);
    }
    n->aktywuj();
    
    assert(wy[0] == 2*we[0] + 2*we[1]);
    
    we[0] = 2;
    we[1] = 3;
    
    n->aktywuj();

    cout << "wy: " << wy[0] << endl;
    assert(wy[0] == 2*we[0] + 2*we[1]);
        
    std::cout << "PASSSSS!!!!!\n**********\n\n" << std::endl;
}

/* Testy uczenia sieci */
void testof_neuralnetwork_propagujBlad()
{
    std::cout << "###################" << endl;
    std::cout << "Test funkcji neuralnetwork_propagujBlad!!!!!\n**********" << std::endl;
    vector<double> wagi, we, wy;
    for (int i=0; i<2; i++) {
        wagi.push_back(1);
        we.push_back(1);
    }
    wy.push_back(999999);
    vector<double> expected;
    expected.push_back(0.7);
    NeuralNetwork* n = new NeuralNetwork(std::string("liniowa"), std::vector<int>{2, 1}, &we, &wy);
    for (int i=0; i<2; i++) {
        n->getLayer(i)->setWagesToAllNeurons(wagi);
    }
    n->aktywuj();
    n->uczSiec(expected);

    cout << "blad na wyjściu: " << n->getLayer(1)->getNeuron(0)->getError() << endl;
    cout << "blad na wejściu1: " << n->getLayer(0)->getNeuron(0)->getError() << endl;
    cout << "blad na wejściu2: " << n->getLayer(0)->getNeuron(1)->getError() << endl;
    //TODO dodać asercję

    vector<double> wy2;
    wy2.push_back(9999999);
    wy2.push_back(9999999);
    n = new NeuralNetwork(std::string("tangens_hiperboliczny"), std::vector<int>{2, 2}, &we, &wy2);
    for (int i=0; i<2; i++) {
        n->getLayer(i)->setWagesToAllNeurons(wagi);
    }
    n->aktywuj();
    expected.push_back(-0.3);
    n->uczSiec(expected);

    we[0] = 4;
    we[1] = -3;
    for (int i=0; i<50; i++) {
        n->aktywuj();
        n->uczSiec(expected);
        cout << "\n\npróba " << i << endl;
        cout << "wyjscie0: " << *n->getLayer(1)->getNeuron(0)->getOutput() << endl;
        cout << "wyjscie1: " << *n->getLayer(1)->getNeuron(1)->getOutput() << endl;
        cout << "blad na wyjściu1: " << n->getLayer(1)->getNeuron(0)->getError() << endl;
        cout << "blad na wyjściu2: " << n->getLayer(1)->getNeuron(1)->getError() << endl;
        cout << "blad na wejściu1: " << n->getLayer(0)->getNeuron(0)->getError() << endl;
        cout << "blad na wejściu2: " << n->getLayer(0)->getNeuron(1)->getError() << endl;
        cout << "WAGIIII" << endl;
        for (int i=0; i<2; ++i) {
            for (int j=0; j<2; j++) {
                cout << "Waga" << i << j;
                cout << " 1: " << n->getLayer(i)->getNeuron(j)->getWage(0) << endl;
                cout << "Waga" << i << j;
                cout << " 2: " << n->getLayer(i)->getNeuron(j)->getWage(1) << endl;
            }
        }
    }
    //TODO dodać asercję
}

void wyznaczanie_czasu_uczenia_sieci() {
    std::cout << "###################" << endl;
    std::cout << "Wyznaczanie czasu uczenia sieci!!!!!\n**********" << std::endl;
    vector<double> wagi5, we, wy, expected;
    int rozmiar_w1 = 5, rozmiar_w2 = 5, rozmiar_wy = 2;
    for (int i=0; i<rozmiar_wy; i++) {
        wy.push_back(99999999);
    }

    for (int i=0; i<rozmiar_w2; i++) {
        wagi5.push_back(1);
        we.push_back(0.2);
    }

    expected.push_back(0.1234567);
    expected.push_back(-0.7654321);

    NeuralNetwork* n = new NeuralNetwork(std::string("tangens_hiperboliczny"), std::vector<int>{rozmiar_w1, rozmiar_w2, rozmiar_wy}, &we, &wy);
    n->getLayer(0)->setWagesToAllNeurons(wagi5);
    n->getLayer(1)->setWagesToAllNeurons(wagi5);
    n->getLayer(2)->setWagesToAllNeurons(wagi5);

    for (int i=0; i<1000; i++) {
        n->aktywuj();
        n->uczSiec(expected);
    }

    cout << "\n\nWYNIKI: " << endl;
    cout << "wyjscie0: " << *n->getLayer(2)->getNeuron(0)->getOutput() << endl;
    cout << "wyjscie1: " << *n->getLayer(2)->getNeuron(1)->getOutput() << endl;
}

