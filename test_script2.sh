#!/bin/bash
mkdir $1

date

#50 pktow
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 0 0 0)>>./$1/out_01.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 0 1 0)>>./$1/out_02.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 1 0 0)>>./$1/out_03.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 1 1 0)>>./$1/out_04.txt &
#200 pktow
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 0 0 0)>>./$1/out_05.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 0 1 0)>>./$1/out_06.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 1 0 0)>>./$1/out_07.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 1 1 0)>>./$1/out_08.txt &


#eta = 1.0
#50 pktow
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 0 0 0)>>./$1/out_11.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 0 1 0)>>./$1/out_12.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 1 0 0)>>./$1/out_13.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 1 1 0)>>./$1/out_14.txt &
#200 pktow
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 0 0 0)>>./$1/out_15.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 0 1 0)>>./$1/out_16.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 1 0 0)>>./$1/out_17.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 1 1 0)>>./$1/out_18.txt &


#eta = 5.0
#50 pktow
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 0 0 0)>>./$1/out_21.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 0 1 0)>>./$1/out_22.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 1 0 0)>>./$1/out_23.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 1 1 0)>>./$1/out_24.txt &
#200 pktow
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 0 0 0)>>./$1/out_25.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 0 1 0)>>./$1/out_26.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 1 0 0)>>./$1/out_27.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 1 1 0)>>./$1/out_28.txt &


# Jedna warstwa
# 4 kolory
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 1 1 0 10)>>./$1/out_1w_01.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 1 1 0 20)>>./$1/out_1w_02.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 1 1 0 30)>>./$1/out_1w_03.txt &

# 2 kolory
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 0 0 0 10)>>./$1/out_1w_04.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 0 0 0 20)>>./$1/out_1w_05.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 0 0 0 30)>>./$1/out_1w_06.txt &

(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 0 1 0 10)>>./$1/out_1w_07.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 0 1 0 20)>>./$1/out_1w_08.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 1.0 0 1 0 30)>>./$1/out_1w_09.txt &

###### eta 0.3
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 1 1 0 10)>>./$1/out_1w_11.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 1 1 0 20)>>./$1/out_1w_12.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 1 1 0 30)>>./$1/out_1w_13.txt &

# 2 kolory
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 0 0 0 10)>>./$1/out_1w_14.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 0 0 0 20)>>./$1/out_1w_15.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 0 0 0 30)>>./$1/out_1w_16.txt &

(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 0 1 0 10)>>./$1/out_1w_17.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 0 1 0 20)>>./$1/out_1w_18.txt &
(python python_src/rozwiazanie_1w.py 200 5000 0.01 0.3 0 1 0 30)>>./$1/out_1w_19.txt 

