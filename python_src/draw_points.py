#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import numpy as np
import re
import matplotlib.pyplot as plt
    
def draw_point(filename):
    img_name = filename[:filename.rfind('.') if ('.' in filename) else None]+".png"
    file = open(filename,'r')
    txt = file.read()
    
    four_colours = "obszar 4" in txt
    print "Analizuje plik, {} obszary".format(4 if four_colours else 2)

    if four_colours:
        obszary = tuple(re.search(r".*(obszar {}:?\n([-]?\d+\.\d+,\s[-]?\d+\.\d+\n)+).*".format(i), txt).group(1).split('\n')[1:-1] for i in range(1,4+1))
    else:
        obszary = tuple(re.search(r".*(obszar {}:?\n([-]?\d+\.\d+,\s[-]?\d+\.\d+\n)+).*".format(i), txt).group(1).split('\n')[1:-1] for i in range(1,2+1))
    dane = tuple(np.zeros([len(obszar),2,]) for obszar in obszary)

    for nr_obszaru,obszar in enumerate(obszary):
        for i,punkt in enumerate(obszar):
            x,y = punkt.split(", ")
            dane[nr_obszaru][i,0]=np.double(x)
            dane[nr_obszaru][i,1]=np.double(y)
    # Odczytanie konkretnego punktu: dane[0][5,:]

    if four_colours:
        plt.plot(dane[0][:,0],dane[0][:,1], 'ro', dane[1][:,0],dane[1][:,1], 'go',
        dane[2][:,0],dane[2][:,1], 'co', dane[3][:,0],dane[3][:,1], 'mo',
        linestyle="None")
    else:
        plt.plot(dane[0][:,0],dane[0][:,1], 'ro', dane[1][:,0],dane[1][:,1], 'go', linestyle="None")
    plt.savefig(img_name)
    print("Zapisalem rysunek z punktami do pliku {}".format(img_name))
    
if __name__=='__main__':
    if len(sys.argv)<2:
        print("Dzialanie skryptu: python draw_points.py nazwa_pliku\nSkrypt wczytuje wynik dzialania skryptu rozwiazanie.py lub rozwiazanie_1w.py i generuje rysunek rozkladu punktow treningowych na plaszczyznie.")
    else:
        draw_point(sys.argv[1])