#!/usr/bin/python
#-*- coding: utf-8 -*-

import random as r
import sys
sys.path.append("build/src")
import wmh_projekt_python_library as n

# Konstruktor ma posiac:
# NeuralNetworkWrapper(string rodzajFunkcjiAktywacji, list listaNeuronowWWarstwach, int rozmiarWejscia, unsigned int rozmiarEpoki = 1000, bool czyDodatkoweWejscie = true)
# Jak widac, rozmiar epoki jest domyslnie ustawiony na 1000 i domyslnie jest tworzone dodatkowe wejscie. Parametr rozmiar wejscia dotyczy tylko wejsc ktore chcemy recznie ustawiac
# Przyklad uzycia:
net = n.NeuralNetworkWrapper("tangens_hiperboliczny", [4,8,4], 2, 100, False)
net = n.NeuralNetworkWrapper("tangens_hiperboliczny", [4,8,4], 2, 100)
net = n.NeuralNetworkWrapper("tangens_hiperboliczny", [4,2], 2, 100)

EXP1 = [1.0, -1.0]
EXP4 = [-1.0, 1.0]

transf = lambda x: x-0.2 if x<0 else x+0.2

for i in range(1,50000):
    x = round(r.random(),2)
    y = round(r.random(),2)
    print i
    if (x==0 or y==0):
        continue
    
    net.setIn([x,y])
    net.aktywuj()
    if (x<0.5):
        net.uczSiec(EXP1)
    else:
        net.uczSiec(EXP4)

print "============="

def count(value):
    x,y=value
    net.setIn([x,y])
    net.aktywuj()
    return net.getOut()

we = [[[round(x*0.1,1),round(y*0.1,1)] for x in range(-10, 10, 1)] for y in range(-10, 10, 1)]
wy = [map(count, v) for v in we]
for y in range(len(we)):
    for x in range(len(we[0])):
        print we[x][y], wy[x][y]

print "============="

# wagi na wejsciu neuronu 0 z warstwy 0
print net.getWagesIn(0,0)
print net.getWagesIn(0,1)
print net.getWagesIn(1,0)
print net.getWagesIn(1,1)
print "============="

# wagi na wyjsciu neuronu 0 z warstwy 0
print net.getWagesOut(0,0)

# bledy z warstwy 2
print net.getErrorsFromLayer(1)