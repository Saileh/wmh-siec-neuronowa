#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import sys
sys.path.append("build/src")
import wmh_projekt_python_library as w
import random as r
import sys


#======================================================================
#        DEFINICJE PRZYDATNYCH(?) FUNKCJI
#======================================================================

def MSE(layer_output, expected):
    error = 0
    for i in range(0, len(layer_output)):
        error = (layer_output[i]-expected[i])*(layer_output[i]-expected[i])/len(layer_output)
    return error

def check_if_mistake(layer_output, expected):
    for i in range(0, len(layer_output)):
        if(layer_output[i] != expected[i]):
            return 1
    return 0

def signum(vector):
    out = []
    for i in range(0,len(vector)):
        if (vector[i] > 0):
            out.append(1)
        elif (vector[i] < 0):
            out.append(-1)
        else:
            out.append(vector[i])
    return out

def testNeuralNetwork(liczba_punktow = 200,  # liczba punktow uczacych z kazdego obszaru:
                      liczba_epok = 5000,  # maksymalna liczba epok
                      expected_error = 0.01,  # zadany blad, do ktorego dazy siec
                      eta = 0.3,  # wspolczynnik uczenia
                      four_colors = True,  # Czy kolory maja być 2 czy 4
                      linear = True,  # Podział liniowy czy nieliniowy?
                      changeWagesAfterEpoque = False,  # Trenowanie co epokę, czy co ileś iteracji?
                      verbose = 5,  # Czy printować dużo?
                      ):
    #======================================================================
    #        PRZYGOTOWANIE PARAMETROW
    #======================================================================
    liczba_punktow, liczba_epok, verbose, = map(int, (liczba_punktow, liczba_epok, verbose,))
    expected_error, eta, = map(float, (expected_error,eta,))
    four_colors, linear, changeWagesAfterEpoque, = map(bool, map(int, (four_colors, linear, changeWagesAfterEpoque,)))
    
    #======================================================================
    #        WYZNACZANIE FUNKCJI
    #======================================================================
    # Wyznaczanie parametrow dwoch funkcji o rownaniu y = ax + b
    # Najpierw losowany jest punkt (x_p,y_p) przeciecia prostych, lezacy na kwadracie: |x|,|y| <= 0.9   <- robie tak, aby zapewnic, ze punkt nie znajdzie sie w samym kacie

    # Nastepnie losowany jest wspolczynnik kierunkowy a z przedzialow <-2,-1> i <1,2> (rozdzielne przedzialy, aby nie pokrywaly sie) oraz liczony jest parametr b zgodnie z rownaniem b = y_p - a * x_p
    y1, y2 = None, None

    if linear:
        x_p = r.uniform(-0.9, 0.9)
        y_p = r.uniform(-0.9, 0.9)
        if verbose>0: print "(x_p, y_p) = (%f,%f)" % (x_p, y_p);

        min_delta = np.pi/4
        delta = r.uniform(min_delta, np.pi-min_delta)
        phi_1 = r.uniform(0, 2*np.pi)
        phi_2 = phi_1 + delta
        a_1, a_2 = np.tan(phi_1), np.tan(phi_2)
        
        b_1, b_2 = y_p - a_1*x_p, y_p - a_2*x_p
        y1, y2 = lambda x: a_1*x + b_1, lambda x: a_2*x + b_2

        if verbose>0: print "funkcja y1 = %f * x + %f" % (a_1, b_1)
        if verbose>0: print "funkcja y2 = %f * x + %f" % (a_2, b_2)
    else:
        x_p = r.uniform(-0.6, 0.6)
        y_p = r.uniform(-0.6, 0.6)
        if verbose>0: print "(x_p, y_p) = (%f,%f)" % (x_p, y_p);

        min_delta = np.pi/4
        delta = r.uniform(min_delta, np.pi-min_delta)
        phi_1 = r.uniform(0, 2*np.pi)
        phi_2 = phi_1 + delta

        a_1, a_2 = r.choice([-1,1])*r.uniform(0.1, 0.4), r.choice([-1,1])*r.uniform(0.1, 0.4)
        b_1, b_2 = np.tan(phi_1), np.tan(phi_2)
        c_1, c_2 = y_p - a_1*x_p, y_p - a_2*x_p
        y1, y2 = lambda x: a_1*x**2 + b_1*x + c_1, lambda x: a_2*x**2 + b_2*x + c_2

        if verbose>0: print("funkcja y1 = %f * x**2 + %f * x + %f" % (a_1, b_1, c_1));
        if verbose>0: print("funkcja y2 = %f * x**2 + %f * x + %f" % (a_2, b_2, c_2));
    #======================================================================
    #        KODOWANIE KOLOROW
    #======================================================================
    colors_number = 4 if four_colors else 2

    EXP_outputs = [[1 if j==i else -1 for i in range(colors_number)] for j in range(colors_number)]

    def nr_koloru(*args):
        y = args[:1+1]
        punkt = args[-1]
        
        podzial = [punkt[1] >= y[0](punkt[0]), punkt[1] >= y[1](punkt[0])]
        if four_colors:
            return sum([podzial[i]*2**i for i in (0,1,)])
        else:
            return sum([podzial[i]*2**i for i in (0,)])
    #======================================================================
    #        LOSOWANIE OBSZAROW OGRANICZONYCH POWYZSZYMI FUNKCJAMI
    #======================================================================
    def generate_training_data():
        obszar1 = []
        obszar2 = []
        obszar3 = []
        obszar4 = []

        if four_colors:
            while (len(obszar1) != liczba_punktow or len(obszar2) != liczba_punktow or len(obszar3) != liczba_punktow or len(obszar4) != liczba_punktow):
                x = 2*r.random()-1
                y = 2*r.random()-1
                if (y >= y1(x) and y >= y2(x)):
                    if (len(obszar1) < liczba_punktow):
                        obszar1.append([x,y])
                    
                elif (y >= y1(x) and y < y2(x)):
                    if (len(obszar2) < liczba_punktow):
                        obszar2.append([x,y])
                    
                elif (y < y1(x) and y < y2(x)):
                    if (len(obszar3) < liczba_punktow):
                        obszar3.append([x,y])
                    
                elif (y < y1(x) and y >= y2(x)):
                    if (len(obszar4) < liczba_punktow):
                        obszar4.append([x,y])
        else:
            while (len(obszar1) != liczba_punktow or len(obszar2) != liczba_punktow):
                x = 2*r.random()-1
                y = 2*r.random()-1
                if (y >= y1(x)):
                    if (len(obszar1) < liczba_punktow):
                        obszar1.append([x,y])

                elif (y < y1(x)):
                    if (len(obszar2) < liczba_punktow):
                        obszar2.append([x,y])
            
            if verbose>5: print "sizes: %d %d %d %d" % (len(obszar1),len(obszar2),len(obszar3),len(obszar4));

        # lista trainingData przechowujaca dane trenujace z wszystkich 4 obszarow
        if four_colors:
            trainingData = obszar1 + obszar2 + obszar3 + obszar4
        else:
            trainingData = obszar1 + obszar2
        return trainingData
        
    trainingData = generate_training_data()
    if four_colors:
        print "obszar 1:"
        for i in range(0, liczba_punktow):
            print "%f, %f" % (trainingData[i][0], trainingData[i][1])
        print "obszar 2:"
        for i in range(liczba_punktow, liczba_punktow*2):
            print "%f, %f" % (trainingData[i][0], trainingData[i][1])
        print "obszar 3"
        for i in range(liczba_punktow*2, liczba_punktow*3):
            print "%f, %f" % (trainingData[i][0], trainingData[i][1])
        print "obszar 4"
        for i in range(liczba_punktow*3, liczba_punktow*4):
            print "%f, %f" % (trainingData[i][0], trainingData[i][1])
    else:
        print "obszar 1:"
        for i in range(0, liczba_punktow):
            print "%f, %f" % (trainingData[i][0], trainingData[i][1])
        print "obszar 2:"
        for i in range(liczba_punktow, liczba_punktow*2):
            print "%f, %f" % (trainingData[i][0], trainingData[i][1])


    print "Len trainingData: %d" % len(trainingData)
    #======================================================================
    #        DEFINICJA SIECI NEURONOWEJ I UCZENIE SIECI
    #======================================================================
    layers_nr = [4,10,colors_number]
    n = w.NeuralNetworkWrapper("tangens_hiperboliczny", layers_nr, 2, len(trainingData))
    n.setEta(eta)
    # lista przechowujaca bledy z poszczegolnych epok podczas uczenia
    all_epoques_errors = []
    # Liczenie bledu polega na policzeniu procenta przypadkow, w ktorych klasyfikacja sie nie powiodla
    # Najpierw nastepuje policzenie funkcji "signum" ("1" gdy x>0, "0" gdy x==0, "-1" gdy x<0; gdzies czytalem, ze w przypadku klasyfikacji powinno sie rzutowac te wartosci)
    # Nastepnie sie sprawdza czy wynik jest bledny (funkcja check_if_mistake), jesli tak, funkcja zwraca "1"
    i1 = 1
    for i in range(1,liczba_epok):
        error_from_epoque = 0
        for j in range(0, len(trainingData)):
            kolor = nr_koloru(y1, y2, trainingData[j])
            EXP = EXP_outputs[kolor]
            output = n.activateAndLearn(trainingData[j], EXP)
            if not changeWagesAfterEpoque:
                if ((i1%3) == 0):
                    n.zmienWagiWarstwy(0)
                if ((i1%4) == 0):
                    n.zmienWagiWarstwy(1)
                if ((i1%10) == 0):
                    n.zmienWagiWarstwy(2)
            i1 = i1 + 1
            error_from_epoque = error_from_epoque + float(check_if_mistake(signum(output), EXP))/len(trainingData)
        error_from_epoque = error_from_epoque
        all_epoques_errors.append(error_from_epoque)
        if verbose>2: print "nr epoki = {}; wartość błędu dla epoki = {}".format(i, error_from_epoque);
        if (all_epoques_errors[-1] < expected_error):
            break
        else:
            if changeWagesAfterEpoque:
                n.zmienWagi()
            trainingData = np.random.permutation(trainingData).tolist()
    # Komunikat, po ilu epokach zakonczono uczenie
    if verbose>0: print "liczba epok: %d" % len(all_epoques_errors);
    #======================================================================
    #        TESTOWANIE
    #======================================================================
    testingData = generate_training_data()
    error_from_test = 0
    for j in range(0, len(testingData)):
        kolor = nr_koloru(y1, y2, testingData[j])
        EXP = EXP_outputs[kolor]
        output = n.activateWithoutLearn(testingData[j])
        error_from_test = error_from_test + float(check_if_mistake(signum(output), EXP))/len(testingData)

    if verbose>0: print("Wielkość danych testowych: {}; Błąd w testach: {}".format(len(testingData), error_from_test));
    if verbose>3: print ("Struktura sieci: {}".format(layers_nr));
    return n  # Funkcja zwraca nauczona siec, ktorej mozna za pomoca metody activateWithoutLearn podac dane w celu przetestowania jej dzialania

    
if __name__=='__main__':
    print("Użycie: parametry skryptu to kolejne parametry funkcji testNeuralNetwork tworzace, trenujacej i testujacej siec")
    print("Podane argumenty: "+str(sys.argv[1:]))
    testNeuralNetwork(*sys.argv[1:])
