#!/bin/bash
mkdir $1

#50 pktow
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 0 0 1)>>./$1/out_01.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 0 1 1)>>./$1/out_02.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 1 0 1)>>./$1/out_03.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 0.3 1 1 1)>>./$1/out_04.txt &
#200 pktow
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 0 0 1)>>./$1/out_05.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 0 1 1)>>./$1/out_06.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 1 0 1)>>./$1/out_07.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 0.3 1 1 1)>>./$1/out_08.txt &

#eta = 1.0
#50 pktow
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 0 0 1)>>./$1/out_11.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 0 1 1)>>./$1/out_12.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 1 0 1)>>./$1/out_13.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 1.0 1 1 1)>>./$1/out_14.txt &
#200 pktow
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 0 0 1)>>./$1/out_15.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 0 1 1)>>./$1/out_16.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 1 0 1)>>./$1/out_17.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 1.0 1 1 1)>>./$1/out_18.txt &

#eta = 5.0
#50 pktow
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 0 0 1)>>./$1/out_11.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 0 1 1)>>./$1/out_12.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 1 0 1)>>./$1/out_13.txt &
(python python_src/rozwiazanie.py 50 5000 0.01 5.0 1 1 1)>>./$1/out_14.txt &
#200 pktow
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 0 0 1)>>./$1/out_15.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 0 1 1)>>./$1/out_16.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 1 0 1)>>./$1/out_17.txt &
(python python_src/rozwiazanie.py 200 5000 0.01 5.0 1 1 1)>>./$1/out_18.txt &