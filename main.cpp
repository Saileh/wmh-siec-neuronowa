#include <iostream>
#include "src/neuron.h"
#include "unit_tests/unittests.h"

std::vector<double> wagi;
std::vector<double> out;

int main(int argc, char **argv) {
    std::cout << "Siedza sobie misie, hop! Siup! Tralala!" << std::endl;
    std::cout << "Smieja im sie pysie, hop! Siup! Tralala!" << std::endl;
    std::cout << "Helloooo!!!!! Babe!!! :D:D:D" << std::endl;

    cout << "\n\nTesty klasy Neuron:" << endl;
    superTestGrzeska();
    testof_neuron_setOutput();
    testof_neuron_setInputs();

    cout << "\n\nTesty klasy NeuralNetwork:" << endl;
    testof_neurallayer_connect();
    testof_neurallayer_connectIn();
    testof_neurallayer_connectIn2();
    testof_neurallayer_connectInFirstLayer();
    testof_neurallayer_connectOut();
    testof_neurallayer_aktywuj();
    testof_neurallayer_aktywuj2();
    testof_neurallayer_aktywuj3();

    cout << "\n\nTesty klasy NeuralNetwork:" << endl;
    testof_neuralnetwork_aktywuj();

    cout << "\n\nTesty uczenia sieci:" << endl;
    testof_neuralnetwork_propagujBlad();
    wyznaczanie_czasu_uczenia_sieci();

    return 0;
}
