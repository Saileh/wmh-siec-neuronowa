/*
 * Created by Albert Malewski. Dec 5th 2015
 */


#include "neuron.h"
#include <iostream>
#include <math.h> 

double Neuron::s_learningCoefficient = 0.3;

Neuron::Neuron() {;}

Neuron::~Neuron() {;}

Neuron::Neuron(std::string fun, std::vector< double > wages, std::vector< double >* input, double* output, double functionParam, unsigned int epoqueSize)
    : m_wages(wages)
    , m_input(input)
    , m_output(output)
{
    m_functionParam = functionParam;
    setFunction(fun);
    m_error = 0;
    m_learningIterationInEpoque = 0;
    m_epoqueErrors = std::vector<std::vector<double> >(m_wages.size(), std::vector<double>(epoqueSize));

    std::cout << "Rozmiar wejscia: " << m_wages.size() << std::endl;
}

void Neuron::setEta ( double eta )
{
    s_learningCoefficient = eta;
}

double Neuron::aktywuj()
{
    double ilocz_skalar = 0;
    if (m_input->size() != m_wages.size()) {
        std::cout << "Error: Wektor wejsciowy i wag powinny byc tych samych rozmiarow!!!" << std::endl;
    std::cout << "size of input: " << m_input->size() << std::endl;
    std::cout << "size of wages: " << m_wages.size() << std::endl;
    return 0;
    }

    for (int i=0; i<m_input->size(); i++)
        ilocz_skalar += m_input->at(i)*m_wages[i];

    if(!m_output)
        return *activationFunction(ilocz_skalar, m_functionParam);
    else {
        *m_output = *activationFunction(ilocz_skalar, m_functionParam);
        return *m_output;
    }
}


void Neuron::setWages(std::vector<double> in)
{
    m_wages = std::move(in);
}

std::vector<double> Neuron::getWages()
{
    return m_wages;
}

void Neuron::setOutput(double* out)
{
    m_output = out;
}

double* Neuron::getOutput()
{
    return m_output;
}

void Neuron::setInputs(std::vector< double >* input)
{
    m_input = input;
}

double& Neuron::getInput ( int i )
{
    return m_input->at(i);
}

void Neuron::setNeuronsFromNextLayer(std::vector< Neuron* >& next)
{
    m_next.resize(next.size());
    for (int i=0; i<next.size(); i++) {
        m_next[i] = next[i];
    }
}

Neuron* Neuron::getNextNeuron ( int i )
{
    if (i<m_next.size())
        return m_next[i];
    else
        return nullptr;
}

double Neuron::getWage(int i)
{
    if (i > m_wages.size()) {
        std::cout << "Brak takiej wagi" << std::endl;
        return 0.0;
    } else
        return m_wages[i];
}

void Neuron::setFunction(std::string fun)
{
    activationFunction.disconnect_all_slots();
    if (!fun.compare("liniowa")) {
        activationFunction.connect(boost::bind(&f_aktywacji_liniowa, _1, _2));
        activationFunction_derivative.connect(boost::bind(&pochodna_f_aktywacji_lin, _1, _2));
    } else if (!fun.compare("heavyside")) {
        activationFunction.connect(boost::bind(&f_aktywacji_heavyside, _1, _2));
        std::cout << "Warning: ustawianie pochodnej jako pochodnej funkcji liniowej" << std::endl;
        activationFunction_derivative.connect(boost::bind(&pochodna_f_aktywacji_lin, _1, _2));
    } else if (!fun.compare("signum")) {
        activationFunction.connect(boost::bind(&f_aktywacji_signum, _1, _2));
        std::cout << "Warning: ustawianie pochodnej jako pochodnej funkcji liniowej" << std::endl;
        activationFunction_derivative.connect(boost::bind(&pochodna_f_aktywacji_lin, _1, _2));
    } else if (!fun.compare("tangens_hiperboliczny")) {
        activationFunction.connect(boost::bind(&f_aktywacji_tan_hiper, _1, _2));
        activationFunction_derivative.connect(boost::bind(&pochodna_f_aktywacji_tanh, _1, _2));
    } else {
        std::cout << "Error: Nie znam takiej opcji. Ustawiam funkcje domyslna (liniowa)" << std::endl;
        activationFunction.connect(boost::bind(&f_aktywacji_liniowa, _1, _2));
        activationFunction_derivative.connect(boost::bind(&pochodna_f_aktywacji_tanh, _1, _2));
    }
}

//TODO: Funkcje aktywacji powinny miec okreslone parametry - trzeba także dorobić pochodne tych funkcji.
double Neuron::f_aktywacji_liniowa(double in, double nachylenie) {
    return in*nachylenie;
}

double Neuron::f_aktywacji_heavyside(double in, double prog) {
    return (in > prog) ? 1.0 : 0;
}

double Neuron::f_aktywacji_signum(double in, double prog) {
    return (in > prog) ? 1.0 : -1.0;
}

double Neuron::f_aktywacji_tan_hiper(double in, double beta) {
    return tanh(in/beta);
}

double Neuron::pochodna_f_aktywacji_lin (double, double nachylenie)
{
    return nachylenie;
}

double Neuron::pochodna_f_aktywacji_tanh (double in, double beta)
{
    return 1/(cosh(in/beta)*cosh(in/beta)*beta);
}

double Neuron::getError()
{
    return m_error;
}

double Neuron::calculateErrorFromFurtherLayer(int indexOfNeuron)
{
    double error = 0.0;
    for (int i=0; i<m_next.size(); i++) {
        error += m_next[i]->getWage(indexOfNeuron)*m_next[i]->getError();
    }
    m_error = error;
    for (int i=0; i<m_input->size(); i++) {
        m_epoqueErrors[i][m_learningIterationInEpoque] = (*activationFunction_derivative(m_input->at(i), m_functionParam))*m_error*m_input->at(i);
    }
    m_learningIterationInEpoque++;

    return m_error;
}


double Neuron::calculateErrorFromExpectedResult(double expected)
{
    m_error = expected - *m_output;
    for (int i=0; i<m_input->size(); i++) {
        m_epoqueErrors[i][m_learningIterationInEpoque] = (*activationFunction_derivative(m_input->at(i), m_functionParam))*m_error*m_input->at(i);
    }
    m_learningIterationInEpoque++;
    return m_error;
}

void Neuron::changeWages()
{
    double delta;
        for (int i=0; i<m_wages.size(); i++) {
            double error = calculateMeanEpoqueErrorForSpecifiedInput(i);
            delta = Neuron::s_learningCoefficient*error;
            m_wages[i] = m_wages[i] + delta;
        }
        m_learningIterationInEpoque = 0;
}

double Neuron::calculateMeanEpoqueErrorForSpecifiedInput(unsigned int n)
{
    double error = 0;
    for (int i=0; i<m_learningIterationInEpoque; i++) {
        error += m_epoqueErrors[n][i];
    }
    error /= (double)m_learningIterationInEpoque;
    return error;
}

