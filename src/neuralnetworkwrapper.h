#ifndef NEURALNETWORKWRAPPER_H
#define NEURALNETWORKWRAPPER_H

#include "neuralnetwork.h"
#include <boost/python.hpp>

/*! @brief Wrapper klasy NeuralNetwork stworzony, pozwalający na używanie kodu z poziomu Pythona */

class NeuralNetworkWrapper
{
public:
    /**
     * Destruktor
     */
    ~NeuralNetworkWrapper() {;}

    /**
     * @brief Konstruktor klasy NeuralNetwork
     * @param typeOfActivationFunction - rodzaj funkcji aktywacji (liniowa, tangens_hiperboliczny, itd.)
     * @param numberOfNeuronsInLayers - pythonowa lista opisująca rozmiar poszczególnych warstw sieci, np [2,3,4]
     * @param sizeOfInput rozmiar wektora wejściowego
     * @param epoqueSize - rozmiar epoki (domyślnie 1000)
     * @param shouldAddExtraConstInput - czy dodać nadmiarowe stałe wejście sieci? True jeśli tak
     */
    NeuralNetworkWrapper(std::string typeOfActivationFunction, boost::python::list& numberOfNeuronsInLayers, int sizeOfInput, unsigned int epoqueSize = 1000, bool shouldAddExtraConstInput = true);

    /**
     * @brief Wyzwala proces aktywacji sieci
     */
    void aktywuj();

    /**
     * @brief Wyzwala proces uczenie sieci na podstawie oczekiwanych wartości wyjściowych
     * @param expected - wektor oczekiwanych wartości wyjściowych
     */
    void uczSiec(boost::python::list& expected);

    /**
     * @brief Wyzwala propagowanie błędu przez sieć na podstawie oczekiwanych wartości wyjściowych
     * @param expected - wektor oczekiwanych wartości wyjściowych
     */
    void propagujBlad(boost::python::list& expected);

    /**
     * @brief Wyzwala zmianę wag podczas uczenia sieci
     */
    void zmienWagi();

    /**
     * @brief Wyzwala zmianę wag tylko w konkretnej warstwie podczas uczenia sieci
     * @param warstwa - numer porządkowy warstwy
     */
    void zmienWagiWarstwy(int warstwa);

    /**
     * @brief Zwraca pythonową listę wyjść sieci
     * @return wyjścia sieci
     */
    boost::python::list getOut();

    /**
     * @brief Ustawia wejście sieci
     * @param in - numer porządkowy warstwy
     */
    void setIn(boost::python::list& in);

    /**
     * @brief Ustawia współczynnik uczenia neuronów
     * @param eta - nowy współczynnik uczenia
     */
    void setEta(double eta);

    /**
     * @brief Zwraca wektor wag wejściowych n-tego neuronu k-tej warstwy
     * @param layerNumber - numer porządkowy neuronu w warstwie
     * @param neuronInLayerNumber - numer porządkowy warstwy
     * @return  pythonowa lista wag wejściowych n-tego neuronu
     */
    boost::python::list getWagesIn(int layerNumber, int neuronInLayerNumber);

    /**
     * @brief Zwraca wektor wag wyjściowych n-tego neuronu k-tej warstwy
     * @param layerNumber - numer porządkowy neuronu w warstwie
     * @param neuronInLayerNumber - numer porządkowy warstwy
     * @return  pythonowa lista wyjściowych n-tego neuronu
     */
    boost::python::list getWagesOut(int layerNumber, int neuronInLayerNumber);

    /**
     * @brief Zwraca wektor błędów popełnionych przez neurony k-tej warstwy
     * @param layerNumber - numer porządkowy warstwy
     * @return pythonowa lista błędów z danej warstwy
     */
    boost::python::list getErrorsFromLayer(int layerNumber);

    /**
     * @brief Wyzwala proces uczenia sieci
     * @param in - wartości na wejściu
     * @param expected - wektor oczekiwanych wartości wyjściowych
     * @return pythonowa lista wyjść sieci
     */
    boost::python::list activateAndLearn(boost::python::list& in, boost::python::list& expected);

    /**
     * @brief Wyzwala proces aktywacji bez uczenia sieci
     * @param in - wartości na wejściu
     * @return pythonowa lista wyjść sieci
     */
    boost::python::list activateWithoutLearn(boost::python::list& in);
private:

    /**
     * @brief Zamienia obiekt typu std::vector na listę pythonową
     * @param vec_obj - numer porządkowy warstwy
     * @return lista pythonowa
     */
    boost::python::list vector_to_python_list(std::vector<double> vec_obj);

    NeuralNetwork* m_network; // wskaźnik na sieć
    std::vector<double>* m_we; // wskaźnik na wektor wejść sieci
    std::vector<double>* m_wy; // wskaźnik na wektor wyjść sieci
};

#endif // NEURALNETWORKWRAPPER_H
