﻿/*
 * Created by Albert Malewski. Dec 15th 2015
 */

#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <vector>
#include <boost/signals2/signal.hpp>
#include <boost/bind.hpp>

#include "neurallayer.h"

/*! @brief Klasa definiująca działanie pełnej sieci neuronowej */

class NeuralNetwork
{
public:
    /**
     * Destruktor
     */
    ~NeuralNetwork() {;}

    /**
     * @brief Konstruktor klasy NeuralNetwork
     * @param typeOfActivationFunction - rodzaj funkcji aktywacji (liniowa, tangens_hiperboliczny, itd.)
     * @param params - wektor opisujący rozmiar poszczególnych warstw sieci, np [2,3,4]
     * @param we - wskaźnik na wektor wejściowy sieci
     * @param wy - wskaźnik na wektor wyjściowy sieci
     * @param epoqueSize - rozmiar epoki (domyślnie 1000)
     */
    NeuralNetwork(std::string typeOfActivationFunction, std::vector<int> params, std::vector<double>* we, std::vector<double>* wy, unsigned int epoqueSize = 1000);

    /**
     * @brief Konstruktor klasy NeuralNetwork
     * @param typeOfActivationFunction - rodzaj funkcji aktywacji (liniowa, tangens_hiperboliczny, itd.)
     */
    NeuralNetwork(std::string typeOfActivationFunction);

    /**
     * @brief Wyzwala proces aktywacji sieci
     */
    void aktywuj();

    /**
     * @brief Wyzwala proces uczenie sieci na podstawie oczekiwanych wartości wyjściowych
     * @param expected - wektor oczekiwanych wartości wyjściowych
     */
    void uczSiec(std::vector<double> expected);

    /**
     * @brief Wyzwala propagowanie błędu przez sieć na podstawie oczekiwanych wartości wyjściowych
     * @param expected - wektor oczekiwanych wartości wyjściowych
     */
    void propagujBlad(std::vector<double> expected);

    /**
     * @brief Wyzwala zmianę wag podczas uczenia sieci
     */
    void zmienWagi();

    /**
     * @brief Wyzwala zmianę wag tylko w konkretnej warstwie podczas uczenia sieci
     *  @param warstwa - numer porządkowy warstwy
     */
    void zmienWagiWarstwy(int warstwa);

    /**
     * @brief Ustawia współczynnik uczenia neuronów
     * @param eta - nowy współczynnik uczenia
     */
    void setEta(double eta);

    /**
     * @brief Zwraca wektor wag wejściowych n-tego neuronu k-tej warstwy
     * @param n - numer porządkowy neuronu w warstwie
     * @param k - numer porządkowy warstwy
     * @return  wektor wag wejściowych n-tego neuronu
     */
    std::vector<double> getWagesIn(int k, int n);

    /**
     * @brief Zwraca wektor wag wyjściowych n-tego neuronu k-tej warstwy
     * @param n - numer porządkowy neuronu w warstwie
     * @param k - numer porządkowy warstwy
     * @return  wektor wag wyjściowych n-tego neuronu
     */
    std::vector<double> getWagesOut(int k, int n);

    /**
     * @brief Zwraca wektor błędów popełnionych przez neurony k-tej warstwy
     * @param k - numer porządkowy warstwy
     * @return wektor błędów
     */
    std::vector<double> getErrorsFromLayer(int k);

    /**
     * @brief Zwraca wskaźnik na i-ty warstwę sieci
     * @param i - numer porządkowy warstwy
     * @return  wskaźnik na i-tą warstwę
     */
    NeuralLayer* getLayer(int i);
private:
    std::vector<NeuralLayer*> m_network; // wskaźnik na wektor warstw
};

#endif // NEURALNETWORK_H
