﻿/*
 * Created by Albert Malewski. Dec 5th 2015
 */


#ifndef NEURON_H
#define NEURON_H

#include <boost/signals2/signal.hpp>
#include <boost/bind.hpp>
#include <vector>
#include <string>

/*! @brief Klasa definiująca działanie pojedynczego neuronu */

class Neuron
{
public:
    /**
     * @brief Konstruktor klasy Neuron
     * @param fun - rodzaj funkcji aktywacji (liniowa, tangens_hiperboliczny, itd.)
     * @param wages - wektor wag wejść neuronu
     * @param input - wskaźnik na wektor wejść neuronu
     * @param output - wskaźnik na wyjście neuronu (czyli gdzie neuron ma zwracać wynik wyjściowy)
     * @param functionParam - współczynnik nachylenia funkcji aktywacji (domyślnie 2)
     * @param epoqueSize - rozmiar epoki (domyślnie 1000)
     */
    Neuron(std::string fun, std::vector< double > wages, std::vector< double >* input, double* output, double functionParam = 2, unsigned int epoqueSize = 1000);

    /**
     * Destruktor
     */
    ~Neuron();

    /**
     * Konstruktor domyślny
     */
    Neuron();

    /**
     * Sygnał, do którego podpinana jest funkcja aktywacji
     */
    boost::signals2::signal<double (double, double)> activationFunction;

    /**
     * Sygnał, do którego podpinana jest pochodna funkcji aktywacji
     */
    boost::signals2::signal<double (double, double)> activationFunction_derivative;

    /**
     * @brief Ustawia wektor wag wejściowych
     * @param wages - wektor wag wejściowych
     */
    void setWages(std::vector<double> wages);

    /**
     * @brief Zwraca wektor wag wejściowych
     * @return - wektor wag wejściowych
     */
    std::vector<double> getWages();

    /**
     * @brief Zwraca wagę i-tego wejścia
     * @param i - numer wejścia
     * @return - wektor wag wejściowych
     */
    double getWage(int i);

    /**
     * @brief Ustawia wskaźnik wyjścia neuronu
     * @param out - wskaźnik, gdzie neuron ma zapisywać swój wynik
     */
    void setOutput(double* out);

    /**
     * @brief Zwraca wskaźnik na wyjście sieci
     * @return - wskaźnik na wyjście sieci
     */
    double* getOutput();

    /**
     * @brief Łączy neuron z neuronami z następnej warstwy (w celu na przykład możliwości pobierania
     *          informacji o błędzie popełnianym przez te neurony). 
     * @param next - wektor wskaźników na neurony z kolejnej warstwy
     */
    void setNeuronsFromNextLayer(std::vector<Neuron*>& next);

    /**
     * @brief Zwraca wskaźnik na i-ty neuron z następnej warstwy
     * @param i - numer neurony z następnej warstwy
     * @return - wskaźnik na i-ty neuron z następnej warstwy
     */
    Neuron* getNextNeuron(int i);

    /**
     * @brief Ustawia wejścia neuronu
     * @param input - wejścia neuronu
     */
    void setInputs(std::vector<double>* input);

    /**
     * @brief Zwraca i-te wejście neuronu
     * @param i - numer wejścia neuronu
     * @return - referenca na i-te wejście
     */
    double& getInput(int i);

    /**
     * @brief Zwraca błąd popełniony przez neuron
     * @return - popełniony błąd
     */
    double getError();

    /**
     * @brief Liczy błąd dla neuronów z warstw ukrytych na podstawie błędu popełnionego przez zadany neuron z warstwy następnej
     * @param indexOfNeuron - numer neuronu z warstwy następnej, na podstawie którego liczony jest błąd cząstkowy
     * @return - wartość błędu
     */
    double calculateErrorFromFurtherLayer(int indexOfNeuron);

    /**
     * @brief Liczy błąd dla neuronów z warstwy wyjściowej na podstawie wartości oczekiwanej jako różnicę między wartością na wyjściu a wartością oczekiwaną
     * @param expected - wartość oczekiwana
     * @return - wartość błędu
     */
    double calculateErrorFromExpectedResult(double expected);

    /**
     * @brief Liczy średni błąd po epoce dla n-tego wejścia
     * @param n - numer wejścia
     * @return - średni błąd dla tego wejścia
     */
    double calculateMeanEpoqueErrorForSpecifiedInput(unsigned int n);

    /**
     * @brief Wyzwala proces zmiany wag
     */
    void changeWages();

    /**
     * @brief Ustawia współczynnik uczenia
     * @param eta - nowy współczynnik uczenia
     */
    static void setEta(double eta);

    /**
     * @brief Wyzwala proces aktywacji neuronu
     * @return - wyjście neuronu
     */
    double aktywuj();

private:
    double* m_output; // wskaźnik na wyjście
    std::vector<double> m_wages; // wektor wag wejściowych
    std::vector<double>* m_input; // m_input wskazuje na wektor wejściowy sieci albo na m_outputs z warstwy poprzedniej
    std::vector<Neuron*> m_next; // wektor wskaźników na neurony z następnej warstwy

    double m_error; // błąd popełniony podczas jednej iteracji uczenia
    double m_functionParam; // parametr funkcji aktywacji nachylenie/próg
    static double s_learningCoefficient; // współczynnik kierunkowy - zmienna statyczna - ten sam współczynnik dla wszystkich neuronów
    std::vector<std::vector<double> > m_epoqueErrors; // tablica dwuwymiarowa, przyporządkowująca danym wejściom błędy popełnione dla tego wejścia podczas całej epoki
    unsigned int m_learningIterationInEpoque; // rozmiar epoki

    /**
     * @brief Ustawia rodzaj funkcji aktywacji
     * @param function - rodzaj funkcji aktywacji, np "liniowa", "tangens_hiperboliczny"
     */
    void setFunction(std::string function);

    /**
     * @brief Liniowa funkcji aktywacji
     * @param in - argument funkcji
     * @param nachylenie - współczynnik kierunkowy
     * @return wartość funkcji aktywacji dla zadanych parametrów
     */
    static double f_aktywacji_liniowa(double in, double nachylenie);

    /**
     * @brief Funkcji aktywacji heavyside'a
     * @param in - argument funkcji
     * @param prog - próg skoku
     * @return wartość funkcji aktywacji dla zadanych parametrów
     */
    static double f_aktywacji_heavyside(double in, double prog);

    /**
     * @brief Funkcji aktywacji signum
     * @param in - argument funkcji
     * @param prog - próg skoku
     * @return wartość funkcji aktywacji dla zadanych parametrów
     */
    static double f_aktywacji_signum(double in, double prog);

    /**
     * @brief Funkcji aktywacji tangens hiperboliczny
     * @param in - argument funkcji
     * @param beta - nachylenie funkcji
     * @return wartość funkcji aktywacji dla zadanych parametrów
     */
    static double f_aktywacji_tan_hiper(double in, double beta);

    /**
     * @brief Pochodna liniowej funkcji aktywacji
     * @param in - argument funkcji
     * @param nachylenie - współczynnik kierunkowy
     * @return wartość pochodnej dla zadanych parametrów
     */
    static double pochodna_f_aktywacji_lin(double, double nachylenie);

    /**
     * @brief Pochodna tangensa hiperbolicznego
     * @param in - argument funkcji
     * @param beta - nachylenie funkcji
     * @return wartość pochodnej dla zadanych parametrów
     */
    static double pochodna_f_aktywacji_tanh(double in, double beta);
};

#endif // NEURON_H
