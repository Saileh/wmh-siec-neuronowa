/*
 * Created by Albert Malewski. 2015
 */

#include "neurallayer.h"
#include "cstdlib"

/* Konstr. i dekonstr. */

NeuralLayer::NeuralLayer(std::string typeOfActivationFunction, int size, int numberOfInputs, double beta, unsigned int epoqueSize)
{
    std::vector<double> wages;
    wages.resize(numberOfInputs);

    m_neurons.resize(size);
    m_outputs = new std::vector<double>(size);
    m_outputs->resize(size);

    for (int i=0; i<m_neurons.size(); i++) {
        for (int i=0; i<wages.size(); i++) {
            wages[i] = (double)((std::rand()%200)-100)/100;    // TODO: Wstepnie wagi inicjalizacyjne są wpisane na sztywno. Trzeba zrobić losowanie.
        }
        m_neurons[i] = new Neuron(typeOfActivationFunction, wages, nullptr, nullptr, beta, epoqueSize);
        m_outputs->at(i) = 0.0;
    }
}

NeuralLayer::~NeuralLayer() {;}

/* Operacje na warstwach */

void NeuralLayer::connectIn(NeuralLayer* layer, std::vector< double >* in)
{
    for (int i=0; i<layer->m_neurons.size(); i++) {
        layer->getNeuron(i)->setInputs(in);
    }
}

void NeuralLayer::connectInFirstLayer(NeuralLayer* layer, std::vector< double >* in)
{
    // FIXME: póki co ta funkcja robi to samo, co connectIn -> trzeba poprawić tak, aby
    // każdy neuron wejściowy był łączony z tylko jednym wejściem.
    for (int i=0; i<layer->m_neurons.size(); i++) {
        layer->getNeuron(i)->setInputs(in);
    }
}

void NeuralLayer::connectOut(NeuralLayer* layer, std::vector< double >* in)
{
    assert(layer->m_neurons.size() == in->size());
    for (int i=0; i<layer->m_neurons.size(); i++) {
        layer->getNeuron(i)->setOutput(&in->at(i));
    }
}

void NeuralLayer::connect(NeuralLayer* first, NeuralLayer* second)
{
    int size_1 = first->m_neurons.size();
    int size_2 = second->m_neurons.size();
    for (int i=0; i<size_1; i++) {
        first->getNeuron(i)->setNeuronsFromNextLayer(second->m_neurons);
    }
    NeuralLayer::connectOut(first, first->m_outputs);
    NeuralLayer::connectIn(second, first->m_outputs); 
}

/* Operacje na neuronach */

Neuron* NeuralLayer::getNeuron(int i)
{
    if (i < m_neurons.size())
        return m_neurons[i];
}

void NeuralLayer::setWagesToSelectedNeuron (int item, std::vector< double > wages)
{
    m_neurons[item]->setWages(wages);
}

void NeuralLayer::setWagesToAllNeurons (std::vector< double > wages )
{
    for (int i=0; i<m_neurons.size(); i++) {
        setWagesToSelectedNeuron(i, wages);
    }
}

void NeuralLayer::setEta ( double eta )
{
    Neuron::setEta(eta);
}

/* Aktywacja i uczenie */

void NeuralLayer::aktywuj()
{
    for (int i=0; i<m_neurons.size(); i++) {
        m_neurons[i]->aktywuj();
    }
}

void NeuralLayer::calculateErrorForLastLayer(std::vector<double> expected)
{
    for (int i=0; i<m_neurons.size(); ++i) {
        m_neurons[i]->calculateErrorFromExpectedResult(expected[i]);
    }
}

void NeuralLayer::calculateErrorForHiddenLayers()
{
    for (int i=0; i<m_neurons.size(); ++i) {
        m_neurons[i]->calculateErrorFromFurtherLayer(i);
    }
}

void NeuralLayer::changeWagesInLayer()
{
    for (int i=0; i<m_neurons.size(); ++i) {
        m_neurons[i]->changeWages();
    }
}

std::vector< double > NeuralLayer::getWagesIn ( int n )
{
    if (n >= m_neurons.size()) {
        std::cout << "Przekroczony zakres - brak takiego neuronu." << std::endl;
        return std::vector<double>{ 0 };
    } else {
        return m_neurons[n]->getWages();
    }
}

std::vector< double > NeuralLayer::getWagesOut ( int n )
{
    std::vector<double> res;

    for (int i=0; i<m_outputs->size(); i++) {
        res.push_back(m_neurons[i]->getWage(n));
    }
    return res;
}

std::vector< double > NeuralLayer::getErrors()
{
    std::vector<double> res;
    for (int i=0; i<m_neurons.size(); i++) {
        res.push_back(m_neurons[i]->getError());
    }
    return res;
}

