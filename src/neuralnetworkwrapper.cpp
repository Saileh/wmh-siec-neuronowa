#include "neuralnetworkwrapper.h"

NeuralNetworkWrapper::NeuralNetworkWrapper(std::string typeOfActivationFunction, boost::python::list& numberOfNeuronsInLayers, int sizeOfInput, unsigned int epoqueSize, bool shouldAddExtraConstInput)
{
    std::vector<int> numberOfNeuronsInLayers_vector;

    for (int i = 0; i<len(numberOfNeuronsInLayers); ++i) {
        numberOfNeuronsInLayers_vector.push_back(boost::python::extract<double>(numberOfNeuronsInLayers[i]));
    }

    if (shouldAddExtraConstInput) {
        m_we = new std::vector<double>(sizeOfInput+1);
        m_we->at(sizeOfInput) = -1.0;
    }
    else
        m_we = new std::vector<double>(sizeOfInput);

    m_wy = new std::vector<double>(numberOfNeuronsInLayers_vector.back());
    m_network = new NeuralNetwork(typeOfActivationFunction, numberOfNeuronsInLayers_vector, m_we, m_wy, epoqueSize);
}

void NeuralNetworkWrapper::aktywuj()
{
    m_network->aktywuj();
}

void NeuralNetworkWrapper::setEta ( double eta )
{
    m_network->setEta(eta);
}

void NeuralNetworkWrapper::uczSiec(boost::python::list& expected_list)
{
    std::vector<double> expected_vec;

    for (int i = 0; i<len(expected_list); ++i) {
        expected_vec.push_back(boost::python::extract<double>(expected_list[i]));
    }
    this->m_network->uczSiec(expected_vec);
}

void NeuralNetworkWrapper::propagujBlad(boost::python::list& expected)
{
    std::vector<double> params;

    for (int i=0; i<len(expected); ++i) {
        params.push_back(boost::python::extract<double>(expected[i]));
    }
    this->m_network->propagujBlad(params);
}

void NeuralNetworkWrapper::zmienWagi()
{
    m_network->zmienWagi();
}

void NeuralNetworkWrapper::zmienWagiWarstwy ( int warstwa )
{
    m_network->zmienWagiWarstwy(warstwa);
}

boost::python::list NeuralNetworkWrapper::getOut()
{
    boost::python::list l;
    return vector_to_python_list(*m_wy);
}

void NeuralNetworkWrapper::setIn ( boost::python::list& in )
{
    if (len(in) > m_we->size()) {
        std::cout << "Nieprawidłowy rozmiar wektora wejściowego" << std::endl;
        return;
    }

    for (int i=0; i<len(in); ++i) {
        m_we->at(i) = boost::python::extract<double>(in[i]);
    }
}

boost::python::list NeuralNetworkWrapper::getWagesIn(int layerNumber, int neuronInLayerNumber)
{
    std::vector<double> wages = m_network->getWagesIn(layerNumber, neuronInLayerNumber);
    return vector_to_python_list(wages);
}

boost::python::list NeuralNetworkWrapper::getWagesOut(int layerNumber, int neuronInLayerNumber)
{
    std::vector<double> wages = m_network->getWagesOut(layerNumber, neuronInLayerNumber);
    return vector_to_python_list(wages);
}

boost::python::list NeuralNetworkWrapper::getErrorsFromLayer(int layerNumber)
{
    std::vector<double> errors = m_network->getErrorsFromLayer(layerNumber);
    return vector_to_python_list(errors);
}

boost::python::list NeuralNetworkWrapper::vector_to_python_list ( std::vector< double > vec_obj )
{
    boost::python::list list_obj;
    for (int i = 0; i<vec_obj.size(); i++) {
        list_obj.append<double>(vec_obj[i]);
    }
    return list_obj;
}

boost::python::list NeuralNetworkWrapper::activateAndLearn ( boost::python::list& in, boost::python::list& expected )
{
    setIn(in);
    aktywuj();
    uczSiec(expected);
    return getOut();
}

boost::python::list NeuralNetworkWrapper::activateWithoutLearn ( boost::python::list& in )
{
    setIn(in);
    aktywuj();
    return getOut();
}

