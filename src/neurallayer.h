﻿/*
 * Created by Albert Malewski. 2015
 */

#ifndef NEURALLAYER_H
#define NEURALLAYER_H

#include <iostream>
#include <vector>
#include "neuron.h"

/**
 * Warstwa zawiera w sobie wektor neuronów (m_neurons) i wektor wyjść z tych neuronów (m_outputs).
 * Łączenie wewnątrz warstw:
 * Każdy neuron z wektora m_neurons jest połączony z tylko jednym wyjściem 
 * (elementem wektora m_outputs) -> łączenie funkcją connectOut. 
 * 
 * Łączenie pomiędzy warstwami:
 * W pierwszej warstwie każdy neuron ma tylko jedno wejście -> łączenie przebiega 
 * poprzez użycie funkcji connectInFirstLayer (FIXME póki co, ta funkcja działa tak samo
 * jak connectIn).
 * W następnych warstwach wejście każdego neuronu (wektor m_inputs z klasy Neuron) 
 * jest połączone z wyjściem z poprzedniej warstwy (wektor m_outputs) -> łączenie 
 * przebiega poprzez użycie funkcji connectIn.
 * 
 * Ponadto każdy neuron jest połączony z neuronami z wartwy następnej (żeby mogły pobierać 
 * informację o błędzie).
 */


class NeuralLayer
{
public:
    /**
     * @brief Konstruktor klasy NeuralLayer
     * @param typeOfActivationFunction - rodzaj funkcji aktywacji (liniowa, tangens_hiperboliczny, itd.)
     * @param size - rozmiar warstwy (liczba neuronów)
     * @param numberOfInputs - rozmiar wejścia
     * @param beta - współczynnik nachylenia funkcji aktywacji (domyślnie 2)
     * @param epoqueSize - rozmiar epoki (domyślnie 1000)
     */ 
    NeuralLayer(std::string typeOfActivationFunction, int size, int numberOfInputs, double beta = 2, unsigned int epoqueSize = 1000);

    /**
     * Konstruktor domyślny
     */
    NeuralLayer();

    /**
     * Destruktor
     */
    ~NeuralLayer();

    /**
     * @brief Wyzwala proces aktywacji neuronów
     */
    void aktywuj();

    /**
     * @brief Łączy wejścia dla wszystkich neuronów warstwie
     * @param layer - warstwa, która ma zostać połączona z wejściem
     * @param in - wejście
     */
    static void connectIn(NeuralLayer* layer, std::vector<double>* in);

    /**
     * @brief Łączy wejścia dla wszystkich neuronów w pierwszej warstwie
     * @param layer - warstwa, która ma zostać połączona z wejściem
     * @param in - wejście
     */
    static void connectInFirstLayer(NeuralLayer* layer, std::vector<double>* in);

    /**
     * @brief Łączy wyjścia dla wszystkich neuronów warstwie
     * @param layer - warstwa, która ma zostać połączona z wejściem
     * @param out - wektor wyjść
     */
    static void connectOut(NeuralLayer* layer, std::vector<double>* out);

    /**
     * @brief Łączy dwie warstwy między sobą
     * @param first - warstwa bliżej wejścia
     * @param second - warstwa bliżej wyjścia
     */
    static void connect(NeuralLayer* first, NeuralLayer* second);

    /**
     * @brief Ustawia takie same wagi dla wszystkich neuronów
     * @param wages - wagi
     */
    void setWagesToAllNeurons(std::vector<double> wages);

    /**
     * @brief Ustawia wektor dla konkretnego neuronu w warstwie
     * @param item - numer porządkowy neuronu
     * @param wages - wektor wag
     */
    void setWagesToSelectedNeuron(int item, std::vector<double> wages);

    /**
     * @brief Wyzwala liczenie błędów neuronów w warstwie wyjściowej na podstawie oczekiwanych wartości wyjściowych
     * @param expected - wektor oczekiwanych wartości wyjściowych
     */
    void calculateErrorForLastLayer(std::vector<double> expected);

    /**
     * @brief Wyzwala liczenie błędów neuronów w warstwach ukrytych
     */
    void calculateErrorForHiddenLayers();

    /**
     * @brief Wyzwala zmianę wag podczas uczenia sieci
     */
    void changeWagesInLayer();

    /**
     * @brief Ustawia współczynnik uczenia neuronów
     * @param eta - nowy współczynnik uczenia
     */
    static void setEta(double eta);

    /**
     * @brief Zwraca wektor wag wejściowych n-tego neuronu warstwy
     * @param n - numer porządkowy neuronu w warstwie
     * @return  wektor wag wejściowych n-tego neuronu
     */
    std::vector<double> getWagesIn(int n);

    /**
     * @brief Zwraca wektor wag na wyjściu n-tego wektora z warstwy poprzedniej
     * @param n - numer porządkowy neuronu w warstwie
     * @return wektor wag na wyjściu n-tego wektora
     */
    std::vector<double> getWagesOut(int n);

    /**
     * @brief Zwraca wektor błędów popełnionych przez neurony z danej warstwy
     * @return wektor błędów
     */
    std::vector<double> getErrors();

    /**
     * @brief Zwraca wskaźnik na i-ty neuron w warstwie
     * @param i - numer porządkowy neuronu w warstwie
     * @return  wskaźnik na i-ty neuron
     */
    Neuron* getNeuron(int i);
private:
    std::vector<Neuron*> m_neurons; // wektor neuronów w danej warstwie.
    std::vector<double>* m_outputs; // wektor wyjść neuronów. funkcja aktywuj() będzie pisywała do tego właśnie wektora.
};

#endif // NEURALLAYER_H
