/*
 * Created by Albert Malewski. Dec 15th 2015
 */

#include "neuralnetwork.h"
#include "ctime"

NeuralNetwork::NeuralNetwork ( std::string typeOfActivationFunction, std::vector< int > params, std::vector< double >* we, std::vector< double >* wy, unsigned int epoqueSize)
{
    srand(std::time(NULL));
    int numb = params.size();
    m_network.resize(numb);

    // pierwsza warstwa ma taki  numberOfInputs jak rozmiar wektora we:
    m_network[0] = new NeuralLayer(typeOfActivationFunction, params[0], we->size(), epoqueSize);
    NeuralLayer::connectIn(m_network[0], we);

    // pozostałe mają taki numberOfInputs jak rozmiar poprzedniej wartwy:
    for (int i=1; i<numb; i++) {
        m_network[i] = new NeuralLayer(typeOfActivationFunction, params[i], params[i-1]);
        NeuralLayer::connect(m_network[i-1], m_network[i]);
    }

    NeuralLayer::connectOut(m_network[numb-1], wy);
}

NeuralLayer* NeuralNetwork::getLayer ( int i )
{
    if (i<m_network.size())
        return m_network[i];
    else
        return nullptr;
}

void NeuralNetwork::aktywuj()
{
    for (int i=0; i<m_network.size(); i++) {
        m_network[i]->aktywuj();
    }
}

void NeuralNetwork::setEta ( double eta )
{
    NeuralLayer::setEta(eta);
}

void NeuralNetwork::uczSiec(std::vector< double > expected)
{
    propagujBlad(expected);
    //zmienWagi();
}

void NeuralNetwork::propagujBlad(std::vector< double > expected)
{
    m_network.back()->calculateErrorForLastLayer(expected);
    for (int i=m_network.size()-2; i>=0; --i) {
        m_network[i]->calculateErrorForHiddenLayers();
    }
}

void NeuralNetwork::zmienWagi()
{
    for (int i=0; i<m_network.size(); i++) {
        m_network[i]->changeWagesInLayer();
    }
}

void NeuralNetwork::zmienWagiWarstwy(int warstwa)
{
    if (warstwa < 0 || warstwa >= m_network.size()) {
        std::cout << "Przekroczony zakres - brak takiej warstwy." << std::endl;
    } else {
        m_network[warstwa]->changeWagesInLayer();
    }
}

std::vector< double > NeuralNetwork::getWagesIn ( int k, int n )
{
    if (k > m_network.size()-1) {
        std::cout << "Przekroczony zakres - brak takiej warstwy." << std::endl;
        return std::vector<double>{ 0 };
    } else {
        return m_network[k]->getWagesIn(n);
    }
}

std::vector< double > NeuralNetwork::getWagesOut ( int k, int n )
{
    if (k >= m_network.size()-1) {
        std::cout << "Przekroczony zakres - brak neuronów lub jest to ostatnia warstwa sieci." << std::endl;
        return std::vector<double>{ 0 };
    } else {
        return m_network[k+1]->getWagesOut(n);
    }
}

std::vector< double > NeuralNetwork::getErrorsFromLayer ( int k )
{
    if (k > m_network.size()-1) {
        std::cout << "Przekroczony zakres - brak takiej warstwy." << std::endl;
        return std::vector<double>{ 0 };
    } else {
        return m_network[k]->getErrors();
    }
}

