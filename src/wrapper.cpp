#include <boost/python.hpp>

#include <boost/python/object/function_object.hpp>
#include <boost/python/object/py_function.hpp>
#include <boost/python/call_method.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "neuralnetworkwrapper.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(wmh_projekt_python_library)
{
    class_<std::vector<double> >("vecD")
        .def(vector_indexing_suite<std::vector<double> >())
        ;

    class_<NeuralNetworkWrapper>("NeuralNetworkWrapper", init<std::string, boost::python::list&, int, optional<unsigned int, bool> >())
        .def("aktywuj", &NeuralNetworkWrapper::aktywuj)
        .def("uczSiec", &NeuralNetworkWrapper::uczSiec)
        .def("setEta", &NeuralNetworkWrapper::setEta)
        .def("propagujBlad", &NeuralNetworkWrapper::propagujBlad)
        .def("zmienWagi", &NeuralNetworkWrapper::zmienWagi)
        .def("zmienWagiWarstwy", &NeuralNetworkWrapper::zmienWagiWarstwy)
        .def("getOut", &NeuralNetworkWrapper::getOut)
        .def("setIn", &NeuralNetworkWrapper::setIn)
        .def("getWagesIn", &NeuralNetworkWrapper::getWagesIn)
        .def("getWagesOut", &NeuralNetworkWrapper::getWagesOut)
        .def("getErrorsFromLayer", &NeuralNetworkWrapper::getErrorsFromLayer)
        .def("activateAndLearn", &NeuralNetworkWrapper::activateAndLearn)
        .def("activateWithoutLearn", &NeuralNetworkWrapper::activateWithoutLearn)
    ;
}